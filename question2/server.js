const fs = require('fs')
const path = require('path')
const server = require('http').createServer()

const file = path.join(__dirname, 'big.file')

let memoryUsage = 0

const highWaterMark = process.env.BUFFER_SIZE || 1

console.log(`Using ${highWaterMark} kb buffer size`)

server.on('request', (req, res) => {
    var stat = fs.statSync(file);

    res.writeHead(200, {
        'Content-disposition': 'attachment; filename=big.file',
        'Content-Type': 'text/plain',
        'Content-Length': stat.size
    });

    var readStream = fs.createReadStream(file, { highWaterMark: highWaterMark * 1024 })

    
    readStream.on('data', (chunk) => {
        memoryUsage = memoryUsage < process.memoryUsage().rss ? process.memoryUsage().rss : memoryUsage
    })
    readStream.on('end', () => {
        memoryUsage = memoryUsage < process.memoryUsage().rss ? process.memoryUsage().rss : memoryUsage

        console.log('Peak memory usage (RSS) is: ' +  (memoryUsage / 1024 / 1024).toFixed(2) + ' mb')
    })

    console.log('Downloading...')

    readStream.pipe(res)
    
});
server.listen(8000)
console.log('Server is running at http://localhost:8000')
