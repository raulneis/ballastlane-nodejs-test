To run this project, first generate the file to download, running `node generate-file.js`.

After that, run the server with `node server.js` and go to `http://localhost:8000` to download the file. The node process will log the memory usage once the download is finished.

By default, a 1kb buffer is used. There is an env variable, `BUFFER_SIZE`, which can be set to the size (in kb) to use for the buffer (see next section for details).

To use a 64kb buffer run `BUFFER_SIZE=64 node server.js`.


# Details

To return the file I use a read stream, and pipe the response to it.
This stream reads the file in chunks and writes each chunk to the response, so we avoid the need of reading the whole file all at once.

This is done with the `createReadStream` method of the `fs` node package.

That function has a parameter called `highWaterMark` which determines the size of the Readable Buffer used underneath. The default value used by node is 64kb. This parameter can be set with the `BUFFER_SIZE` environment variable.

One can set a low value, like 1kb to lower the memory footprint. Of course, a lower value here makes the download slower, since it requires more disk reads and more copy operations in memory, so in an real world case one should test this value for an optimal memory-consumption/performance ratio.

The test I did where the following:
- For 1kb buffer, peak RSS was 29,75 mb
- For 64kb buffer, peak RSS was 112,95 mb

I use RSS as a measure here since it's the resident set size (which is the actual physical memory used).
