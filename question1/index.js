/**
 * Original function was:
 *
 * async function(req, res) {
 *   try {
 *     const a = await someOtherFunction();
 *     const b = await somethingElseFunction();
 *     res.send("result");
 *   } catch(err) {
 *     res.send(err.stack);
 *   }
 * }
 *
 * I've rewritten it to use promises instead of async/await.
 * As the original function returns nothing, this new one's
 * not required to return a promise.
 */

function(req, res) {
  Promise.all([
    someOtherFunction(),
    somethingElseFunction()
  ]).then(({0: a, 1: b}) => {
    res.send("result" + a + b)
  }).catch((err) => {
    res.send(err.stack);
  })
}
