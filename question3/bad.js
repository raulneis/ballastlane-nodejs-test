const foo = function () {
    this.someObject.someAttribute += 4;
    console.log(this.someObject);
};
const foo2 = () => {
    this.someObject.someAttribute += 4;
    console.log(this.someObject);
};
const context = {
    someObject: {
        someAttribute: 2
    }
};
foo.call(context); // IT WORKS
foo2.call(context); // IT CRASHES
