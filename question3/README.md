The file `bad.js` contains the original code that is broken, while `fixed.js` contains the fixed code.

# Details

JS functions always run within a _context_, which is bind to the `this` keyword. `this` is always an object (except in the global scope running in strict mode, where it's _undefined_).

Historically, `this` has always been dynamically bind, which means that it's value is resolved at runtime. The _usual_ behaviour is that it's bind to the object that invoked the function where `this` is used. (Note here that the context is bound to the invoker, so when reading a function one have no clue of what's in `this`).

The `call` method on the function prototype (as well as `apply` and `bind`) allows the programmer to override the default value of `this`.

So, when we run `foo.call(context)`, the content of `context` is used as the value of `this` within `foo` execution for this case.

ES6 introduzed a new syntax to define functions called _arrow functions_. This new syntax also carries a change on the semantics of the `this` keyword and how it is bound. When a function is defined with this syntax, `this` is _lexically_ bound to the originating context (literally where the code is written), so it cannot be overwritten during the execution.

So, the problem with `foo2.call(context)` is that `context` is not bound to `this` within `foo2`, so `someObject` is undefined.

To solve the problem one can just change the way `foo2` is defined to a function expression.

Instead of:

`const foo2 = () => { ... }`

we should define it as:

`const foo2 = function() { ... }`

